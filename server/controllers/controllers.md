# Controllers in encrypted_thoughts
## Design Philosophy
Controllers should receive request objects from the route  
It should "clean" the requests and make sure they are formatted properly from HTTP web request.  
If not "clean" return a 400 response

Controllers should only be soley responsible for cleaning request objects and calling model methods.
Then it should cater responses to return back to the route method.  
All methods in this controller should be "public" by default.  
If you need util methods that shouldn't be accessible by the route, make a util functions file.

