const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const express = require("express");
const cors = require('cors');
const app = express();
const port = 8001;
const routes = require('./routes');

// connect to Mongo Daemon
mongoose.connect("mongodb://mongo/encrypted_thoughts_development", {
  useNewUrlParser: true,
  useUnifiedTopology: true
});

const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error: "));
console.log('trying to connect to mongodb')
db.once("open", function () {
// Body-parser middleware
  app.use(cors());
  app.use(bodyParser.urlencoded({extended: false}))
  app.use(bodyParser.json())

  app.get("/", (req, res) => {
    res.send({data: 'hello'});
  });

  app.listen(port, () => {
    console.log("App is listening at", port, "but if you're using docker it might be running a different port on your host machine");
  });

  // adding routes
  app.use('/api', routes);
});

