# Routes in encrypted_thoughts
## Design Philosophy
Routes dictate which controller the request should be parsed by.  
The responsibility of the route is to purely pass the request object to the controller.  
It shouldn't do anything else.