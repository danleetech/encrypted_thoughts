const router = require('express').Router();
const thinkerRoutes = require('./api/thinker');
const authRoutes = require('./api/auth')

router.use('/thinker', thinkerRoutes);
router.use('/auth', authRoutes)

module.exports = router;
